# PokerTable

## How to enjoy

Open [index.html](src/html/index.html) on a web browser.

## Status

Functional user interface. Single player game starts and can move through one round (restarts when game is reloaded).
Simple AI in singleplayer mode. Users can connect to the multiplayer lobby, but gameplay through multiplayer is not 
yet available. 

## Next step

* **Michael**
* Help implement a multiplayer capability to connect more than single players. 
* Improve end game UI. 

* **Dylan**
* Catalog each part of the code which currently impedes me from implementing
  proper round structure in the game, then refactor this code. This will likely 
  fix many bugs and allow for proper round implementation.

* **Russell**
* Implement mulitplayer

* **Brett**
* Improve Ai opponent
* make check/call and bet/raise buttons contextual to the state of the game
* disperse money at the end of the round

### Work Scope reduction and reallocation of Sprint 2:
NOTE: Over estimation of task complexity and time required changes to our board for sprint 2.
Implementing proper round iterations and button functions was more complex and time intensive
then fist thought. To accommodate for this mistake the task POK-27 had a subsequent refinement task
POK-39 added. The same was applied to task POK-26 with an added refinement task POK-44. Due to 
more work being done on EpicPOK-28 (Playable Game) Epic POK-38 (Single Player Opponent) was reduced
in scope for Sprint 2. This entailed moving Tasks POK-40 to the Backlog and changing POK-39 to its 
current description and requirements.

### Work Scope reduction of Sprint 1:
NOTE: Poor task creation was a major hurdle for sprint 1. Over/under estimation on completion time,
and over/under simplification of individual task complexity hindered progress significantly. Some
tasks were not fleshed out enough and in time to understand what truly should be done for that task.


## Table of contents
* [Project Members](#project-members)
* [What is PokerTable?](#what-is-pokertable)
* [Who is this for?](#who-is-this-for)
* [Why are we making this?](#why-are-we-making-this)
* [Technologies](#technologies)
* [Features](#features)

![PokerTable Logo](img/Poker%20Table%20Logo.png)


## Project Members
* Michael Kammer
* Dylan Ray
* Russell Hernandez Ruiz
* Brett Owen


## What is PokerTable?
PokerTable is a website that lets people play poker online. This game can either
be played in a single player mode or multiplayer mode.


## Who is this for?
People who want to play Poker with friends online while not risking Covid at a casino.


## Why are we making this?
PokerTable wants users to be able to enjoy a simple, easy game of poker with no setup or fee's over the internet.


## Technologies
* JavaScript
* Canvas
* HTML5
* go


## Features/Accomplishments (Sprint3)
* **Michael**
* Added Winning hand function in Game.js
  Created and implemented a winning hand function that gathers every players hands and 
  adds the table's cards to a comparitive array that rates the hand giving each player a score. 
  This then checks for folded hands and rates the player at 0 if folded. After, the funciton 
  will go through and determine the winner of the round based on the hand ratings given from the
  hand rating function and class. Changes can be found in the [Game.js](src/html/Game.js) 
* Inseted Winning hand function call in main.js
  Created a function call in the main class to correctly and timely call the winning hand function class 
  to call the function at the correct time to make sure all players were rated at the end of the game
  vs before the right time. Changes can be found in the [main.js](src/html/main.js)
  
  

* **Dylan**
* Implemented a function in the Game.js file which uses the HandRatingClass.js code to create a final hand 
  ranking for a player, given a player as an argument. The function constructs the porper Json object from
  the players feilds and passes those tothe HandRatingClass.js file for ranking. The returned ranking could
  be further used elsewhere in the code to implement a showdown to tell who has won the game.
  The code can be found in [Game.js](src/html/Game.js)
* Added a folder containing all the sound effects now implemented in the game, includeing sounds
  for each button, card deal, deck shuffle, and general background music. All files are .mp3's and
  can be found at [Audio](src/html/Audio).
* Integrated all the previously mentioned sound effects files into the code and made each sound
  play after the apropiarte action is performed by the game of the user. I used the javaScript
  Audio object in conjunction with our click action listener to implement the sounds. The changes 
  to the code and be found at [Game.js](src/html/Game.js), and [Main.js](src/html/Main.js)

* **Russell**
* Almost finished the back-end. See [server](src/server/). There is a
  server, and the front-end will connect to it and send some messages.
  It has most of the game logic, mostly what is left is actually sending
  messages to and from the server and client. The game is also online,
  connect to <http://hyperlife.xyz/pokertable>. Use `go run . [ipaddr]`
  from inside the [server](src/server/) directory to run it.

* **Brett**
* Implemented UI functions, such as bet and call when the bet and call buttons are pressed. 
* Fully implemented the fold feature, preventing folded users from making moves. 
* Improved the UI to show the current max bet and let players make specific bets using the + and - buttons
* implemented betting and tracking the current amount of cash players have. 

### TODO

Play a full poker game online

* Featuring all rulesets for many different games of poker.
* Includes systems for betting against other players.
* Allow players to play rounds with friends!

Poker Hand Recogniztion

* Create a function which takes in a players hand and returns a unique number which ranks 
  the hand and then comopares it to all other player hand to determine who has won the round.

Private Rooms

* Allow users to meet in individual poker sessions to play with friends or anyone with a room code.

Customize your PokerTable with unique unlockables

* Allow users to get different aesthetic upgrades with points earned through playing the game.
* Upgrades include background colors, chip decals, and Deck Designs.

Gambling free

* To protect our users, the website does not include or condone using real world money for playing the game.

